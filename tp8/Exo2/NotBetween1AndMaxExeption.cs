﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    public class NotBetween1AndMaxExeption : Exception
    {
        public NotBetween1AndMaxExeption() : base ("Le nombre entré n'est pas compris entre 1 et max")
        { }
    }
}
