﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    public class ImpossibleConversionToIntExeption : Exception
    {
        public ImpossibleConversionToIntExeption() : base ("Impossible de convertir en int")
        { }
    }
}
