﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    public class InferiorTo2Exeption : Exception
    {
        public InferiorTo2Exeption() : base("This number musn't be inferior of equal to 1")
        { }
    }
}
