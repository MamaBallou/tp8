﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Console.WriteLine(menu.QuestionReponse());
            int max;
            string max_string;
            do
            {
                Console.Write("Saisir le nombre d'option du menu : ");
                max_string = Console.ReadLine();
            } while (!int.TryParse(max_string, out max));
            Menu menu2 = new Menu(max);
            Console.WriteLine(menu2.QuestionReponse());
            Console.ReadKey();
        }
    }
}
