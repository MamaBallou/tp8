﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2
{
    public class Menu
    {
        public List<string> lstChoix;
        public string question;

        public Menu()
        {
            this.lstChoix = new List<string>();
            this.question = "Quel choix voulez vous faire ?";
            lstChoix.Add("Dire bonjour");
            lstChoix.Add("Cite le Seigneur des Anneaux");
            lstChoix.Add("Fuir");
        }

        public Menu(int nb)
        {
            this.lstChoix = new List<string>();
            this.question = "Quel choix voulez vous faire ?";
            for (int compt = 0; compt < nb; compt++)
            {
                string choix;
                Console.Write("Saisir le choix {0} : ", compt);
                choix = Console.ReadLine();
                lstChoix.Add(choix);
            }
        }

        public int inputChoice(ref int n)
        {
            try
            {
                string n_string;
                Console.Write("Saisir le choix d'action :");
                n_string = Console.ReadLine();
                if (!int.TryParse(n_string, out n)) throw new ImpossibleConversionToIntExeption();
                if (n <= 1) throw new InferiorTo2Exeption();
                if ((n < 1) && (n > this.lstChoix.Capacity)) throw new NotBetween1AndMaxExeption();
                return n;
            }
            catch (ImpossibleConversionToIntExeption ex)
            {
                Console.WriteLine("Erreur : {0}", ex.Message);
                return -1;
            }
            catch (InferiorTo2Exeption ex)
            {
                Console.WriteLine("Erreur : {0}", ex.Message);
                return -2;
            }
            catch (NotBetween1AndMaxExeption ex)
            {
                Console.WriteLine("Erreur : {0}", ex.Message);
                return -3;
            }
        }

        public void Affiche()
        {
            int compt = 0;
            Console.WriteLine(this.question);
            foreach (string chaine in this.lstChoix)
            {
                Console.WriteLine("{0} : {1}", compt + 1, chaine);
                compt++;
            }
        }

        public int QuestionReponse()
        {
            this.Affiche();
            int n = 0;
            try
            {
                if (this.lstChoix.Capacity <= 1) throw new InferiorTo2Exeption();
            }
            catch(InferiorTo2Exeption ex)
            {
                Console.WriteLine("Erreur : {0}", ex.Message);
            }
            return inputChoice(ref n);
        }
    }
}
