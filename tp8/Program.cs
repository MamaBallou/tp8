﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp8
{
    public class Program
    {
        public static double RacineCarree(double valeur)
        {
            try
            {
                if (valeur <= 0)
                    throw new ArgumentOutOfRangeException("valeur", "Le paramètre doit être positif");
                return Math.Sqrt(valeur);
            }
            catch
            {
                Console.WriteLine("Impossible d'obtenir la racie de ce nombre");
                return -1;
            }
        }
        
        static void Main(string[] args)
        {
            List<int> values = new List<int> { 17, 12, 15, 38, 29, 157, 89, -22, 0, 5 };
            MultipleDivision mltd = new MultipleDivision(values);

            //TODO: Ajouter saisie index_dividende, puis saisie index_diviseur
            int index_dividende, index_diviseur;
            string index_dividende_str, index_diviseur_str;
            do
            {
                Console.Write("Saisir le dividende :");
                index_dividende_str = Console.ReadLine();
            } while (!int.TryParse(index_dividende_str, out index_dividende));
            do
            {
                Console.Write("Saisir le diviseur :");
                index_diviseur_str = Console.ReadLine();
            } while (!int.TryParse(index_diviseur_str, out index_diviseur));

            //TODO: Call mltd.division(index_dividende, index_diviseur)
            double resultat = mltd.Division(index_dividende, index_diviseur);
            //TODO: Show Result
            Console.WriteLine("La division donne {0}", resultat);
            Console.ReadKey();
        }
    }
}
