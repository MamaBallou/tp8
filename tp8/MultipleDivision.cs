﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tp8
{
    public class MultipleDivision
    {
        private List<int> values;
        public MultipleDivision(List<int> values)
        {
            this.values = values;
        }

        public double Division(int index_dividende, int index_diviseur)
        {
            double quotient;
            try
            {
                quotient = this.values[index_dividende] / this.values[index_diviseur];
            }
            catch
            {
                Console.WriteLine("Division par 0 impossible");
                return -1;
            }
            return quotient;
        }
    }
}
